# Home Monitoring

The aim of this project is to provide APIs and an interface for home monitoring, recording statistics from different sensors for human consumption to understand what's happening around them.

## Internet Connectivity

This captures where the server can connect to the internet, with a poll time of 10 seconds, and records this to the local data store instance.

The frontend then charts this, whilst the server generates the relevant buckets for the requested data.

## Development

The project is split into the `app` and `server` folders.

The `app` folder contains an Angular project. To develop this the app can be started with `npm start` and built with `npm run build`, the latter command will build the Angular project and copy it into the `server` folder.

The `server` folder contains a NodeJS express server. This server both hosts the Angular frontend and the API. The server can be started with `npm start` and then the frontend is hosted at `localhost:5000`. This should be accessible on the local network via the hosts IP / hostname.