import { EChartsOption } from 'echarts';
import { DateTime } from 'luxon';

export function getChartConfig() {
  const chartConfig: EChartsOption = {
    tooltip: {
      trigger: 'item',
      formatter: (v: any) => {
        const [dateMs, internetConnectivity] = v.data;

        return `${getFormattedDateTime(dateMs)} <b>${getConnectivityString(
          internetConnectivity
        )}</b>`;
      },
    },
    grid: {
      containLabel: true,
    },
    xAxis: {
      type: 'time',
      name: 'Time',
      nameLocation: 'middle',
      nameGap: 35,
      nameTextStyle: {
        fontWeight: 'bold',
        fontSize: '14px',
      },
      axisLine: {
        show: true,
        onZero: false,
      },
      axisLabel: {
        showMaxLabel: true,
        formatter: (v: number) => getFormattedDateTime(+v),
      },
    },
    yAxis: {
      type: 'value',
      name: 'Internet connectivity',
      nameLocation: 'middle',
      nameGap: 95,
      nameTextStyle: {
        fontWeight: 'bold',
        fontSize: '14px',
      },
      axisLine: {
        show: true,
      },
      axisLabel: {
        formatter: (v: number) => getConnectivityString(v),
      },
      minInterval: 1,
      max: 1,
      min: -1,
    },
    series: [
      {
        name: 'Internet connectivity',
        color:  '#1a759f',
        type: 'line',
        step: 'end',
        data: [],
      },
    ],
  };

  return chartConfig;
}

function getConnectivityString(value: number) {
  switch (value) {
    case 1:
      return 'Connected';
    case 0:
      return 'Disconnected';
    case -1:
    default:
      return 'No data';
  }
}

function getFormattedDateTime(dateMs: number) {
  const dt = DateTime.fromMillis(dateMs);

  return dt.toFormat('LLL d, HH:mm');
}
