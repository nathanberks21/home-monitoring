import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlineConnectivityComponent } from './online-connectivity.component';

describe('OnlineConnectivityComponent', () => {
  let component: OnlineConnectivityComponent;
  let fixture: ComponentFixture<OnlineConnectivityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnlineConnectivityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlineConnectivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
