import { Component, OnDestroy, OnInit } from '@angular/core';
import { EChartsOption } from 'echarts';
import { Subject, timer } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ApiService } from 'src/app/services/api.service';
import { getChartConfig } from './online-connectivity.utils';

@Component({
  selector: 'app-online-connectivity',
  templateUrl: './online-connectivity.component.html',
  styleUrls: ['./online-connectivity.component.scss'],
})
export class OnlineConnectivityComponent implements OnDestroy, OnInit {
  chartConfig: EChartsOption = getChartConfig();
  timePeriodHours = [1, 6, 12, 24, 48, 72, 168];
  bucketSizeSeconds = [10, 30, 60, 300, 900, 1800, 3600, 10800, 43200];
  selectedTimePeriodHours = 24;
  selectedBucketSizeSeconds = 60;
  lastUpdatedAt?: Date;

  private readonly destroySubs$ = new Subject();

  constructor(private apiService: ApiService) {}

  ngOnInit() {
    timer(0, 60000)
      .pipe(takeUntil(this.destroySubs$))
      .subscribe(() => this.updateChartWithNewTimePeriods());
  }

  ngOnDestroy() {
    this.destroySubs$.next();
    this.destroySubs$.complete();
  }

  updateChartWithNewTimePeriods() {
    this.apiService
      .getOnlineConnectivity$(
        this.selectedTimePeriodHours,
        this.selectedBucketSizeSeconds
      )
      .pipe(takeUntil(this.destroySubs$))
      .subscribe(({ bucketedValues }) => {
        if (!Array.isArray(this.chartConfig.series)) {
          return;
        }

        this.chartConfig.series[0].data = bucketedValues
          .reverse()
          .map(({ timeMs, canConnectToInternet }) => [
            timeMs,
            canConnectToInternet,
          ]);

        this.chartConfig = { ...this.chartConfig };

        this.lastUpdatedAt = new Date();
      });
  }

  getFormattedTimePeriodText(period: number) {
    if (period < 24) {
      return period === 1 ? '1 hour' : `${period} hours`;
    }

    const days = period / 24;

    return days === 1 ? '1 day' : `${days} days`;
  }

  getFormattedBucketSize(bucketSize: number) {
    if (bucketSize < 60) {
      return `${bucketSize} seconds`;
    }

    const minutes = bucketSize / 60;

    if (minutes < 60) {
      return minutes === 1 ? '1 minute' : `${minutes} minutes`;
    }

    const hours = minutes / 60;

    return hours === 1 ? '1 hour' : `${hours} hours`;
  }
}
