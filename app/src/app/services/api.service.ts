import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GetOnlineConnectivityResponse } from 'src/app/types/api.types';

const endpoint = environment.apiEndpoint;

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  getOnlineConnectivity$(timePeriodHours: number, bucketSizeSeconds: number) {
    return this.http.get<GetOnlineConnectivityResponse>(
      `${endpoint}/internet-uptime`,
      { params: { timePeriodHours, bucketSizeSeconds } }
    );
  }
}
