export interface GetOnlineConnectivityResponse {
  bucketedValues: OnlineConnectivityBucket[];
}

export interface OnlineConnectivityBucket {
  timeMs: number;
  canConnectToInternet: -1 | 0 | 1; // -1 = no value, 0 = no internet, 1 = internet
}
