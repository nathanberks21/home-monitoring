var express = require('express');
var router = express.Router();

/* Render Angular frontend */
router.get('/', function(req, res) {
  res.render('index');
});

module.exports = router;
