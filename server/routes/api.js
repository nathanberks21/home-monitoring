const express = require("express");
const router = express.Router();
const { getBucketedValues } = require("../utils/internet-monitor.utils");

router.get("/internet-uptime", async function (req, res) {
  const { timePeriodHours, bucketSizeSeconds } = req.query;

  const bucketedValues = await getBucketedValues(
    timePeriodHours,
    bucketSizeSeconds
  );

  return res.status(200).json({ bucketedValues });
});

module.exports = router;
