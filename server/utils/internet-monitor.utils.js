const {
  getInternetConnectivityDataPoints,
} = require("../services/internet-monitor.service");

function createEmptyBuckets(timePeriodHours, bucketSizeSeconds) {
  const timePeriodSeconds = timePeriodHours * 60 * 60;
  const numberOfBuckets = Math.round(timePeriodSeconds / bucketSizeSeconds);

  const buckets = new Array(numberOfBuckets).fill(undefined);
  const timeNowMs = new Date().getTime();

  return buckets.map((_, idx) => ({
    timeMs: timeNowMs - idx * bucketSizeSeconds * 1000,
    canConnectToInternet: -1, // -1 = no value, 0 = no internet, 1 = internet
  }));
}

async function getBucketedValues(timePeriodHours, bucketSizeSeconds) {
  const data = await getInternetConnectivityDataPoints(timePeriodHours);
  const buckets = createEmptyBuckets(timePeriodHours, bucketSizeSeconds);
  const timeNowMs = new Date().getTime();
  const bucketSizeMs = bucketSizeSeconds * 1000;

  data.forEach(({ timestamp, canConnect }) => {
    const dataPointTimeMs = new Date(timestamp).getTime();
    const timeDifferenceMs = timeNowMs - dataPointTimeMs;

    const bucketIndex = Math.floor(timeDifferenceMs / bucketSizeMs);

    try {
      // Only update the the value if the bucket hasn't registered no internet (0)
      if (buckets[bucketIndex].canConnectToInternet !== 0) {
        buckets[bucketIndex].canConnectToInternet = canConnect ? 1 : 0;
      }
    } catch {
      console.error(
        `Error writing to bucket with index ${bucketIndex}. Buckets array has length ${buckets.length}.`
      );
    }
  });

  return buckets;
}

module.exports = {
  getBucketedValues,
};
