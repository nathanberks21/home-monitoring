const path = require("path");
const DataStore = require("nedb-promises");
const isOnline = require("is-online");

let dataStore;

const POLL_INTERVAL = 10000;

function initialise() {
  console.log("Starting internet monitoring …");

  dataStore = DataStore.create(path.join(__dirname, "internet-monitor.db"));

  setInterval(() => storeInternetAvailability(), POLL_INTERVAL);

  console.log("Started internet monitoring.");
}

async function storeInternetAvailability() {
  const timestamp = new Date().toISOString();
  let canConnect = false;

  try {
    canConnect = await isOnline();
  } catch {}

  try {
    await dataStore.insert({ timestamp, canConnect });
  } catch (e) {
    console.error("Error writing to data store!");
    console.error(e);
  }
}

async function getInternetConnectivityDataPoints(timePeriodHours) {
  const filterBackToDate = new Date();
  const timePeriodMs = timePeriodHours * 60 * 60 * 1000;

  filterBackToDate.setTime(filterBackToDate.getTime() - timePeriodMs);

  const data = await dataStore.find({
    timestamp: { $gte: filterBackToDate.toISOString() },
  });

  return data;
}

module.exports = {
  getInternetConnectivityDataPoints,
  initialise,
};
